"""
Odd words are copied in reverse order
while even words are merely echoed.
For example, the input string
  : whats the matter with kansas.

becomes
  : whats eht matter htiw kansas.

The problem is further restricted in that
the characters must be read
and printed one at a time.
"""
global output
output = ''
global word_count
word_count = 0
global text
text = ''
global current_character
current_character = ''
global previous_character
previous_character = ''


def foo():
    global word_count
    global current_character
    character = read()  # read one at a time
    not_finished = character != '.'
    if word_count % 2 == 1:  # is odd
        if current_character not in (' ', '.'):
            not_finished = foo()
        if current_character == ' ':
            word_count += 1
    else:
        if character == ' ':
            word_count += 1
    write(character)  # write one at a time
    return not_finished

def write(result):
    global output
    output += result

def read():
    global text
    global current_character
    global previous_character
    previous_character = current_character
    if text:
        character, text = text[0], text[1:]
        current_character = character
    return previous_character

def fun(original):
    global word_count
    global output
    global text
    text = original
    word_count = 0
    output = ''
    read()  #  required to allow lookahead
    while foo():
        pass
    return output
