import pytest
import oddword
from oddword import fun
# discussion
# what is minimal input?
# how to enforce constraint?
# introduce word count
# decide to not account for multiple spaces
# refactor to separate read and write
# move read and write into foo
# implement reverse using recursion
# deal with lookahead problem
#
def test_minimal_input():
    assert fun('a.') == 'a.'

def test_trivial_two_word_case():
    assert fun('a b.') == 'a b.'
    assert oddword.word_count == 1

@pytest.mark.skip
def test_tmp_trivial_two_word_case():
    assert fun('a b.') == 'a .b'
    assert oddword.word_count == 1

def test_trivial_three_word_case():
    assert fun('a b c.') == 'a b c.'
    assert oddword.word_count == 2

@pytest.mark.skip
def test_tmp_trivial_three_word_case():
    assert fun('a b c.') == 'a  bc.'
    assert oddword.word_count == 2

def test_nontrivial_single_word():
    assert fun('ab.') == 'ab.'

def test_nontrivial_two_word_case():
    assert fun('a bc.') == 'a cb.'

@pytest.mark.skip
def test_tmp_nontrivial_two_word_case():
    assert fun('a bc.') == 'a .cb'